from django.db import models


class Product(models.Model):
    product_name = models.CharField(max_length=50)
    product_description = models.TextField(default='')
    price = models.CharField(max_length=50)

    def __str__(self):
        return self.product_name


class ProductImages(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE, related_name='images')
    image = models.ImageField(upload_to='media/')
