from django.contrib import admin

from DNApp.models import Product, ProductImages

admin.site.register(Product)
admin.site.register(ProductImages)
