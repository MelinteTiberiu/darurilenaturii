from django.core.paginator import Paginator
from django.shortcuts import render
from django.views.generic import TemplateView

from DNApp.models import Product


class HomeTemplateView(TemplateView):
    template_name = 'homepage.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        products = Product.objects.all()
        context['products'] = products
        return context
