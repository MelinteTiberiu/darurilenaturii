from django.urls import path

from DNApp import views

urlpatterns = [
    path('', views.HomeTemplateView.as_view(), name='home-page')
]
